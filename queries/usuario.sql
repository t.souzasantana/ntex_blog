--! cadastra_usuario
INSERT into usuario (
    nome, email, cpf, senha
)
VALUES (
    :nome, :email, :cpf, :senha
)
returning *;

--! login_usuario
SELECT *
FROM usuario 
WHERE cpf = :cpf;

--! user_by_id
SELECT *
FROM usuario 
WHERE id = :id;

--! user_by_cpf
SELECT *
FROM usuario 
WHERE cpf = :cpf;

--! user_by_email
SELECT *
FROM usuario 
WHERE email = :email;

--! atualiza_usuario
UPDATE usuario
SET nome = :nome, email = :email
WHERE id = :id
returning *;

--! todos_usuarios
SELECT *
FROM usuario
ORDER BY id;

--! excluir_usuario
DELETE
FROM usuario
WHERE id = :id;

--! inativar_usuario
UPDATE usuario
SET is_active = 'F'
WHERE id = :id;

--! ativar_usuario
UPDATE usuario
SET is_active = 'T'
WHERE id = :id;
