--! todas_permissoes
SELECT 
permissao.perfil_id AS perfil_id,
permissao.funcionalidade_id AS funcionalidade_id,
perfil.perfil AS perfil,
perfil.descricao AS perfil_descricao,
funcionalidade.funcionalidade AS funcionalidade,
funcionalidade.descricao AS funcionalidade_descricao
FROM permissao
JOIN perfil ON permissao.perfil_id = perfil.id
JOIN funcionalidade ON permissao.funcionalidade_id = funcionalidade.id;

--! lista_permissoes
SELECT *
FROM permissao;

--! insert_permissao
INSERT 
into permissao
(perfil_id, funcionalidade_id)
VALUES (:perfil_id, :funcionalidade_id)
ON CONFLICT (perfil_id, funcionalidade_id)
DO NOTHING
RETURNING *;


--! permissao_perfil
select funcionalidade_id
from permissao p 
where p.perfil_id = :perfil_id;

--! delete_permissao
delete 
from permissao 
where funcionalidade_id = :funcionalidade_id 
and perfil_id = :perfil_id;
