--! todos_grupos
SELECT 
grupo.usuario_id AS usuario_id,
grupo.perfil_id AS perfil_id,
usuario.nome AS usuario_nome,
perfil.perfil AS perfil,
perfil.descricao AS perfil_descricao
FROM grupo
JOIN usuario ON grupo.usuario_id = usuario.id
JOIN perfil ON grupo.perfil_id = perfil.id;


--! cadastra_grupo
INSERT into grupo (
    usuario_id, perfil_id
)
VALUES (
    :usuario_id, :perfil_id
)
returning *;