--! todas_funcionalidades
SELECT *
FROM funcionalidade;

--! atualiza_funcionalidade
UPDATE funcionalidade
SET funcionalidade = :funcionalidade, descricao = :descricao
WHERE id = :id
returning *;

