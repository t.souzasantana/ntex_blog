## Criando banco de dados

Cria o banco de dados do seu projeto, substituindo <newdatabase> pelo nome desejado:

    sudo -u postgres psql -c 'create database <newdatabase>;'

Cria um superusuário que pode criar bancos de dados, substituindo <newuser> e <newpassword>:

    sudo -u postgres psql -c "create role <newuser> login superuser password '<newpassword>';"

