use ntex::web;
use dotenv::dotenv;
//add minijinjua stuff
use ntex_files;
use ntex_session::CookieSession;
use thiago::controllers;
use thiago::utils::db::create_pool;
use thiago::utils::AppState;



#[ntex::main]
async fn main() -> std::io::Result<()> {

    // Load environment variables from .env file
    dotenv().ok(); 
    // get app port from environment variable parsed to integer
    let port: u16 = std::env::var("APP_PORT").expect("APP_PORT not set in .env file").parse().unwrap();
    // get number of workers (threads) from environment variable parsed to usize
    let workers :usize = std::env::var("WORKERS").expect("WORKERS not set in .env file").parse().unwrap();
    let cpool = create_pool().await.unwrap();
 
    // Start http server
    web::HttpServer::new(move || {
        web::App::new()
            .state(AppState {
                pool: cpool.clone(),
            })            
            .wrap(
                CookieSession::signed(&[0; 32]) // <- create cookie based session middleware
                    .secure(false),
            )
            .configure(controllers::frontend::configure)
            .configure(controllers::dashboard::auth::configure)
            .service(ntex_files::Files::new("/static", "static/"))

    })
    .workers(workers)
    .bind(("127.0.0.1", port))?
    .run()
    .await
}
