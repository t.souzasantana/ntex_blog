// This file was generated with `cornucopia`. Do not modify.

#[allow(clippy::all, clippy::pedantic)] #[allow(unused_variables)]
#[allow(unused_imports)] #[allow(dead_code)] pub mod types { }#[allow(clippy::all, clippy::pedantic)] #[allow(unused_variables)]
#[allow(unused_imports)] #[allow(dead_code)] pub mod queries
{ pub mod funcionalidade
{ use futures::{{StreamExt, TryStreamExt}};use futures; use cornucopia_async::GenericClient;#[derive( Debug)] pub struct AtualizaFuncionalidadeParams<T1: cornucopia_async::StringSql,T2: cornucopia_async::StringSql,> { pub funcionalidade: T1,pub descricao: T2,pub id: i32,}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct TodasFuncionalidades
{ pub id : i32,pub funcionalidade : String,pub descricao : String,}pub struct TodasFuncionalidadesBorrowed<'a> { pub id : i32,pub funcionalidade : &'a str,pub descricao : &'a str,}
impl<'a> From<TodasFuncionalidadesBorrowed<'a>> for TodasFuncionalidades
{
    fn from(TodasFuncionalidadesBorrowed { id,funcionalidade,descricao,}: TodasFuncionalidadesBorrowed<'a>) ->
    Self { Self { id,funcionalidade: funcionalidade.into(),descricao: descricao.into(),} }
}pub struct TodasFuncionalidadesQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> TodasFuncionalidadesBorrowed,
    mapper: fn(TodasFuncionalidadesBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> TodasFuncionalidadesQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(TodasFuncionalidadesBorrowed) -> R) ->
    TodasFuncionalidadesQuery<'a,C,R,N>
    {
        TodasFuncionalidadesQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct AtualizaFuncionalidade
{ pub id : i32,pub funcionalidade : String,pub descricao : String,}pub struct AtualizaFuncionalidadeBorrowed<'a> { pub id : i32,pub funcionalidade : &'a str,pub descricao : &'a str,}
impl<'a> From<AtualizaFuncionalidadeBorrowed<'a>> for AtualizaFuncionalidade
{
    fn from(AtualizaFuncionalidadeBorrowed { id,funcionalidade,descricao,}: AtualizaFuncionalidadeBorrowed<'a>) ->
    Self { Self { id,funcionalidade: funcionalidade.into(),descricao: descricao.into(),} }
}pub struct AtualizaFuncionalidadeQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> AtualizaFuncionalidadeBorrowed,
    mapper: fn(AtualizaFuncionalidadeBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> AtualizaFuncionalidadeQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(AtualizaFuncionalidadeBorrowed) -> R) ->
    AtualizaFuncionalidadeQuery<'a,C,R,N>
    {
        AtualizaFuncionalidadeQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}pub fn todas_funcionalidades() -> TodasFuncionalidadesStmt
{ TodasFuncionalidadesStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM funcionalidade")) } pub struct
TodasFuncionalidadesStmt(cornucopia_async::private::Stmt); impl TodasFuncionalidadesStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
) -> TodasFuncionalidadesQuery<'a,C,
TodasFuncionalidades, 0>
{
    TodasFuncionalidadesQuery
    {
        client, params: [], stmt: &mut self.0, extractor:
        |row| { TodasFuncionalidadesBorrowed { id: row.get(0),funcionalidade: row.get(1),descricao: row.get(2),} }, mapper: |it| { <TodasFuncionalidades>::from(it) },
    }
} }pub fn atualiza_funcionalidade() -> AtualizaFuncionalidadeStmt
{ AtualizaFuncionalidadeStmt(cornucopia_async::private::Stmt::new("UPDATE funcionalidade
SET funcionalidade = $1, descricao = $2
WHERE id = $3
returning *")) } pub struct
AtualizaFuncionalidadeStmt(cornucopia_async::private::Stmt); impl AtualizaFuncionalidadeStmt
{ pub fn bind<'a, C:
GenericClient,T1:
cornucopia_async::StringSql,T2:
cornucopia_async::StringSql,>(&'a mut self, client: &'a  C,
funcionalidade: &'a T1,descricao: &'a T2,id: &'a i32,) -> AtualizaFuncionalidadeQuery<'a,C,
AtualizaFuncionalidade, 3>
{
    AtualizaFuncionalidadeQuery
    {
        client, params: [funcionalidade,descricao,id,], stmt: &mut self.0, extractor:
        |row| { AtualizaFuncionalidadeBorrowed { id: row.get(0),funcionalidade: row.get(1),descricao: row.get(2),} }, mapper: |it| { <AtualizaFuncionalidade>::from(it) },
    }
} }impl <'a, C: GenericClient,T1: cornucopia_async::StringSql,T2: cornucopia_async::StringSql,> cornucopia_async::Params<'a,
AtualizaFuncionalidadeParams<T1,T2,>, AtualizaFuncionalidadeQuery<'a, C,
AtualizaFuncionalidade, 3>, C> for AtualizaFuncionalidadeStmt
{
    fn
    params(&'a mut self, client: &'a  C, params: &'a
    AtualizaFuncionalidadeParams<T1,T2,>) -> AtualizaFuncionalidadeQuery<'a, C,
    AtualizaFuncionalidade, 3>
    { self.bind(client, &params.funcionalidade,&params.descricao,&params.id,) }
}}pub mod grupos
{ use futures::{{StreamExt, TryStreamExt}};use futures; use cornucopia_async::GenericClient;#[derive(Clone,Copy, Debug)] pub struct CadastraGrupoParams<> { pub usuario_id: i32,pub perfil_id: i32,}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct TodosGrupos
{ pub usuario_id : i32,pub perfil_id : i32,pub usuario_nome : String,pub perfil : String,pub perfil_descricao : String,}pub struct TodosGruposBorrowed<'a> { pub usuario_id : i32,pub perfil_id : i32,pub usuario_nome : &'a str,pub perfil : &'a str,pub perfil_descricao : &'a str,}
impl<'a> From<TodosGruposBorrowed<'a>> for TodosGrupos
{
    fn from(TodosGruposBorrowed { usuario_id,perfil_id,usuario_nome,perfil,perfil_descricao,}: TodosGruposBorrowed<'a>) ->
    Self { Self { usuario_id,perfil_id,usuario_nome: usuario_nome.into(),perfil: perfil.into(),perfil_descricao: perfil_descricao.into(),} }
}pub struct TodosGruposQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> TodosGruposBorrowed,
    mapper: fn(TodosGruposBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> TodosGruposQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(TodosGruposBorrowed) -> R) ->
    TodosGruposQuery<'a,C,R,N>
    {
        TodosGruposQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,Copy)] pub struct CadastraGrupo
{ pub usuario_id : i32,pub perfil_id : i32,}pub struct CadastraGrupoQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> CadastraGrupo,
    mapper: fn(CadastraGrupo) -> T,
} impl<'a, C, T:'a, const N: usize> CadastraGrupoQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(CadastraGrupo) -> R) ->
    CadastraGrupoQuery<'a,C,R,N>
    {
        CadastraGrupoQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}pub fn todos_grupos() -> TodosGruposStmt
{ TodosGruposStmt(cornucopia_async::private::Stmt::new("SELECT 
grupo.usuario_id AS usuario_id,
grupo.perfil_id AS perfil_id,
usuario.nome AS usuario_nome,
perfil.perfil AS perfil,
perfil.descricao AS perfil_descricao
FROM grupo
JOIN usuario ON grupo.usuario_id = usuario.id
JOIN perfil ON grupo.perfil_id = perfil.id")) } pub struct
TodosGruposStmt(cornucopia_async::private::Stmt); impl TodosGruposStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
) -> TodosGruposQuery<'a,C,
TodosGrupos, 0>
{
    TodosGruposQuery
    {
        client, params: [], stmt: &mut self.0, extractor:
        |row| { TodosGruposBorrowed { usuario_id: row.get(0),perfil_id: row.get(1),usuario_nome: row.get(2),perfil: row.get(3),perfil_descricao: row.get(4),} }, mapper: |it| { <TodosGrupos>::from(it) },
    }
} }pub fn cadastra_grupo() -> CadastraGrupoStmt
{ CadastraGrupoStmt(cornucopia_async::private::Stmt::new("INSERT into grupo (
    usuario_id, perfil_id
)
VALUES (
    $1, $2
)
returning *")) } pub struct
CadastraGrupoStmt(cornucopia_async::private::Stmt); impl CadastraGrupoStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
usuario_id: &'a i32,perfil_id: &'a i32,) -> CadastraGrupoQuery<'a,C,
CadastraGrupo, 2>
{
    CadastraGrupoQuery
    {
        client, params: [usuario_id,perfil_id,], stmt: &mut self.0, extractor:
        |row| { CadastraGrupo { usuario_id: row.get(0),perfil_id: row.get(1),} }, mapper: |it| { <CadastraGrupo>::from(it) },
    }
} }impl <'a, C: GenericClient,> cornucopia_async::Params<'a,
CadastraGrupoParams<>, CadastraGrupoQuery<'a, C,
CadastraGrupo, 2>, C> for CadastraGrupoStmt
{
    fn
    params(&'a mut self, client: &'a  C, params: &'a
    CadastraGrupoParams<>) -> CadastraGrupoQuery<'a, C,
    CadastraGrupo, 2>
    { self.bind(client, &params.usuario_id,&params.perfil_id,) }
}}pub mod perfil
{ use futures::{{StreamExt, TryStreamExt}};use futures; use cornucopia_async::GenericClient;#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct TodosPerfis
{ pub id : i32,pub perfil : String,pub descricao : String,}pub struct TodosPerfisBorrowed<'a> { pub id : i32,pub perfil : &'a str,pub descricao : &'a str,}
impl<'a> From<TodosPerfisBorrowed<'a>> for TodosPerfis
{
    fn from(TodosPerfisBorrowed { id,perfil,descricao,}: TodosPerfisBorrowed<'a>) ->
    Self { Self { id,perfil: perfil.into(),descricao: descricao.into(),} }
}pub struct TodosPerfisQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> TodosPerfisBorrowed,
    mapper: fn(TodosPerfisBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> TodosPerfisQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(TodosPerfisBorrowed) -> R) ->
    TodosPerfisQuery<'a,C,R,N>
    {
        TodosPerfisQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct PerfilById
{ pub id : i32,pub perfil : String,pub descricao : String,}pub struct PerfilByIdBorrowed<'a> { pub id : i32,pub perfil : &'a str,pub descricao : &'a str,}
impl<'a> From<PerfilByIdBorrowed<'a>> for PerfilById
{
    fn from(PerfilByIdBorrowed { id,perfil,descricao,}: PerfilByIdBorrowed<'a>) ->
    Self { Self { id,perfil: perfil.into(),descricao: descricao.into(),} }
}pub struct PerfilByIdQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> PerfilByIdBorrowed,
    mapper: fn(PerfilByIdBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> PerfilByIdQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(PerfilByIdBorrowed) -> R) ->
    PerfilByIdQuery<'a,C,R,N>
    {
        PerfilByIdQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}pub fn todos_perfis() -> TodosPerfisStmt
{ TodosPerfisStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM perfil")) } pub struct
TodosPerfisStmt(cornucopia_async::private::Stmt); impl TodosPerfisStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
) -> TodosPerfisQuery<'a,C,
TodosPerfis, 0>
{
    TodosPerfisQuery
    {
        client, params: [], stmt: &mut self.0, extractor:
        |row| { TodosPerfisBorrowed { id: row.get(0),perfil: row.get(1),descricao: row.get(2),} }, mapper: |it| { <TodosPerfis>::from(it) },
    }
} }pub fn perfil_by_id() -> PerfilByIdStmt
{ PerfilByIdStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM perfil 
WHERE id = $1")) } pub struct
PerfilByIdStmt(cornucopia_async::private::Stmt); impl PerfilByIdStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
id: &'a i32,) -> PerfilByIdQuery<'a,C,
PerfilById, 1>
{
    PerfilByIdQuery
    {
        client, params: [id,], stmt: &mut self.0, extractor:
        |row| { PerfilByIdBorrowed { id: row.get(0),perfil: row.get(1),descricao: row.get(2),} }, mapper: |it| { <PerfilById>::from(it) },
    }
} }}pub mod permissao
{ use futures::{{StreamExt, TryStreamExt}};use futures; use cornucopia_async::GenericClient;#[derive(Clone,Copy, Debug)] pub struct InsertPermissaoParams<> { pub perfil_id: i32,pub funcionalidade_id: i32,}#[derive(Clone,Copy, Debug)] pub struct DeletePermissaoParams<> { pub funcionalidade_id: i32,pub perfil_id: i32,}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct TodasPermissoes
{ pub perfil_id : i32,pub funcionalidade_id : i32,pub perfil : String,pub perfil_descricao : String,pub funcionalidade : String,pub funcionalidade_descricao : String,}pub struct TodasPermissoesBorrowed<'a> { pub perfil_id : i32,pub funcionalidade_id : i32,pub perfil : &'a str,pub perfil_descricao : &'a str,pub funcionalidade : &'a str,pub funcionalidade_descricao : &'a str,}
impl<'a> From<TodasPermissoesBorrowed<'a>> for TodasPermissoes
{
    fn from(TodasPermissoesBorrowed { perfil_id,funcionalidade_id,perfil,perfil_descricao,funcionalidade,funcionalidade_descricao,}: TodasPermissoesBorrowed<'a>) ->
    Self { Self { perfil_id,funcionalidade_id,perfil: perfil.into(),perfil_descricao: perfil_descricao.into(),funcionalidade: funcionalidade.into(),funcionalidade_descricao: funcionalidade_descricao.into(),} }
}pub struct TodasPermissoesQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> TodasPermissoesBorrowed,
    mapper: fn(TodasPermissoesBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> TodasPermissoesQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(TodasPermissoesBorrowed) -> R) ->
    TodasPermissoesQuery<'a,C,R,N>
    {
        TodasPermissoesQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,Copy)] pub struct ListaPermissoes
{ pub perfil_id : i32,pub funcionalidade_id : i32,}pub struct ListaPermissoesQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> ListaPermissoes,
    mapper: fn(ListaPermissoes) -> T,
} impl<'a, C, T:'a, const N: usize> ListaPermissoesQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(ListaPermissoes) -> R) ->
    ListaPermissoesQuery<'a,C,R,N>
    {
        ListaPermissoesQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,Copy)] pub struct InsertPermissao
{ pub perfil_id : i32,pub funcionalidade_id : i32,}pub struct InsertPermissaoQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> InsertPermissao,
    mapper: fn(InsertPermissao) -> T,
} impl<'a, C, T:'a, const N: usize> InsertPermissaoQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(InsertPermissao) -> R) ->
    InsertPermissaoQuery<'a,C,R,N>
    {
        InsertPermissaoQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}pub struct I32Query<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> i32,
    mapper: fn(i32) -> T,
} impl<'a, C, T:'a, const N: usize> I32Query<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(i32) -> R) ->
    I32Query<'a,C,R,N>
    {
        I32Query
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}pub fn todas_permissoes() -> TodasPermissoesStmt
{ TodasPermissoesStmt(cornucopia_async::private::Stmt::new("SELECT 
permissao.perfil_id AS perfil_id,
permissao.funcionalidade_id AS funcionalidade_id,
perfil.perfil AS perfil,
perfil.descricao AS perfil_descricao,
funcionalidade.funcionalidade AS funcionalidade,
funcionalidade.descricao AS funcionalidade_descricao
FROM permissao
JOIN perfil ON permissao.perfil_id = perfil.id
JOIN funcionalidade ON permissao.funcionalidade_id = funcionalidade.id")) } pub struct
TodasPermissoesStmt(cornucopia_async::private::Stmt); impl TodasPermissoesStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
) -> TodasPermissoesQuery<'a,C,
TodasPermissoes, 0>
{
    TodasPermissoesQuery
    {
        client, params: [], stmt: &mut self.0, extractor:
        |row| { TodasPermissoesBorrowed { perfil_id: row.get(0),funcionalidade_id: row.get(1),perfil: row.get(2),perfil_descricao: row.get(3),funcionalidade: row.get(4),funcionalidade_descricao: row.get(5),} }, mapper: |it| { <TodasPermissoes>::from(it) },
    }
} }pub fn lista_permissoes() -> ListaPermissoesStmt
{ ListaPermissoesStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM permissao")) } pub struct
ListaPermissoesStmt(cornucopia_async::private::Stmt); impl ListaPermissoesStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
) -> ListaPermissoesQuery<'a,C,
ListaPermissoes, 0>
{
    ListaPermissoesQuery
    {
        client, params: [], stmt: &mut self.0, extractor:
        |row| { ListaPermissoes { perfil_id: row.get(0),funcionalidade_id: row.get(1),} }, mapper: |it| { <ListaPermissoes>::from(it) },
    }
} }pub fn insert_permissao() -> InsertPermissaoStmt
{ InsertPermissaoStmt(cornucopia_async::private::Stmt::new("INSERT 
into permissao
(perfil_id, funcionalidade_id)
VALUES ($1, $2)
ON CONFLICT (perfil_id, funcionalidade_id)
DO NOTHING
RETURNING *")) } pub struct
InsertPermissaoStmt(cornucopia_async::private::Stmt); impl InsertPermissaoStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
perfil_id: &'a i32,funcionalidade_id: &'a i32,) -> InsertPermissaoQuery<'a,C,
InsertPermissao, 2>
{
    InsertPermissaoQuery
    {
        client, params: [perfil_id,funcionalidade_id,], stmt: &mut self.0, extractor:
        |row| { InsertPermissao { perfil_id: row.get(0),funcionalidade_id: row.get(1),} }, mapper: |it| { <InsertPermissao>::from(it) },
    }
} }impl <'a, C: GenericClient,> cornucopia_async::Params<'a,
InsertPermissaoParams<>, InsertPermissaoQuery<'a, C,
InsertPermissao, 2>, C> for InsertPermissaoStmt
{
    fn
    params(&'a mut self, client: &'a  C, params: &'a
    InsertPermissaoParams<>) -> InsertPermissaoQuery<'a, C,
    InsertPermissao, 2>
    { self.bind(client, &params.perfil_id,&params.funcionalidade_id,) }
}pub fn permissao_perfil() -> PermissaoPerfilStmt
{ PermissaoPerfilStmt(cornucopia_async::private::Stmt::new("select funcionalidade_id
from permissao p 
where p.perfil_id = $1")) } pub struct
PermissaoPerfilStmt(cornucopia_async::private::Stmt); impl PermissaoPerfilStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
perfil_id: &'a i32,) -> I32Query<'a,C,
i32, 1>
{
    I32Query
    {
        client, params: [perfil_id,], stmt: &mut self.0, extractor:
        |row| { row.get(0) }, mapper: |it| { it },
    }
} }pub fn delete_permissao() -> DeletePermissaoStmt
{ DeletePermissaoStmt(cornucopia_async::private::Stmt::new("delete 
from permissao 
where funcionalidade_id = $1 
and perfil_id = $2")) } pub struct
DeletePermissaoStmt(cornucopia_async::private::Stmt); impl DeletePermissaoStmt
{ pub async fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
funcionalidade_id: &'a i32,perfil_id: &'a i32,) -> Result<u64, tokio_postgres::Error>
{
    let stmt = self.0.prepare(client).await?;
    client.execute(stmt, &[funcionalidade_id,perfil_id,]).await
} }impl <'a, C: GenericClient + Send + Sync, >
cornucopia_async::Params<'a, DeletePermissaoParams<>, std::pin::Pin<Box<dyn futures::Future<Output = Result<u64,
tokio_postgres::Error>> + Send + 'a>>, C> for DeletePermissaoStmt
{
    fn
    params(&'a mut self, client: &'a  C, params: &'a
    DeletePermissaoParams<>) -> std::pin::Pin<Box<dyn futures::Future<Output = Result<u64,
    tokio_postgres::Error>> + Send + 'a>>
    { Box::pin(self.bind(client, &params.funcionalidade_id,&params.perfil_id,)) }
}}pub mod usuario
{ use futures::{{StreamExt, TryStreamExt}};use futures; use cornucopia_async::GenericClient;#[derive( Debug)] pub struct CadastraUsuarioParams<T1: cornucopia_async::StringSql,T2: cornucopia_async::StringSql,T3: cornucopia_async::StringSql,T4: cornucopia_async::StringSql,> { pub nome: T1,pub email: T2,pub cpf: T3,pub senha: T4,}#[derive( Debug)] pub struct AtualizaUsuarioParams<T1: cornucopia_async::StringSql,T2: cornucopia_async::StringSql,> { pub nome: T1,pub email: T2,pub id: i32,}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct CadastraUsuario
{ pub id : i32,pub nome : String,pub email : String,pub cpf : String,pub senha : String,pub is_active : String,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}pub struct CadastraUsuarioBorrowed<'a> { pub id : i32,pub nome : &'a str,pub email : &'a str,pub cpf : &'a str,pub senha : &'a str,pub is_active : &'a str,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}
impl<'a> From<CadastraUsuarioBorrowed<'a>> for CadastraUsuario
{
    fn from(CadastraUsuarioBorrowed { id,nome,email,cpf,senha,is_active,created_at,created_by,updated_at,updated_by,}: CadastraUsuarioBorrowed<'a>) ->
    Self { Self { id,nome: nome.into(),email: email.into(),cpf: cpf.into(),senha: senha.into(),is_active: is_active.into(),created_at,created_by,updated_at,updated_by,} }
}pub struct CadastraUsuarioQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> CadastraUsuarioBorrowed,
    mapper: fn(CadastraUsuarioBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> CadastraUsuarioQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(CadastraUsuarioBorrowed) -> R) ->
    CadastraUsuarioQuery<'a,C,R,N>
    {
        CadastraUsuarioQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct LoginUsuario
{ pub id : i32,pub nome : String,pub email : String,pub cpf : String,pub senha : String,pub is_active : String,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}pub struct LoginUsuarioBorrowed<'a> { pub id : i32,pub nome : &'a str,pub email : &'a str,pub cpf : &'a str,pub senha : &'a str,pub is_active : &'a str,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}
impl<'a> From<LoginUsuarioBorrowed<'a>> for LoginUsuario
{
    fn from(LoginUsuarioBorrowed { id,nome,email,cpf,senha,is_active,created_at,created_by,updated_at,updated_by,}: LoginUsuarioBorrowed<'a>) ->
    Self { Self { id,nome: nome.into(),email: email.into(),cpf: cpf.into(),senha: senha.into(),is_active: is_active.into(),created_at,created_by,updated_at,updated_by,} }
}pub struct LoginUsuarioQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> LoginUsuarioBorrowed,
    mapper: fn(LoginUsuarioBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> LoginUsuarioQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(LoginUsuarioBorrowed) -> R) ->
    LoginUsuarioQuery<'a,C,R,N>
    {
        LoginUsuarioQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct UserById
{ pub id : i32,pub nome : String,pub email : String,pub cpf : String,pub senha : String,pub is_active : String,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}pub struct UserByIdBorrowed<'a> { pub id : i32,pub nome : &'a str,pub email : &'a str,pub cpf : &'a str,pub senha : &'a str,pub is_active : &'a str,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}
impl<'a> From<UserByIdBorrowed<'a>> for UserById
{
    fn from(UserByIdBorrowed { id,nome,email,cpf,senha,is_active,created_at,created_by,updated_at,updated_by,}: UserByIdBorrowed<'a>) ->
    Self { Self { id,nome: nome.into(),email: email.into(),cpf: cpf.into(),senha: senha.into(),is_active: is_active.into(),created_at,created_by,updated_at,updated_by,} }
}pub struct UserByIdQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> UserByIdBorrowed,
    mapper: fn(UserByIdBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> UserByIdQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(UserByIdBorrowed) -> R) ->
    UserByIdQuery<'a,C,R,N>
    {
        UserByIdQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct UserByCpf
{ pub id : i32,pub nome : String,pub email : String,pub cpf : String,pub senha : String,pub is_active : String,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}pub struct UserByCpfBorrowed<'a> { pub id : i32,pub nome : &'a str,pub email : &'a str,pub cpf : &'a str,pub senha : &'a str,pub is_active : &'a str,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}
impl<'a> From<UserByCpfBorrowed<'a>> for UserByCpf
{
    fn from(UserByCpfBorrowed { id,nome,email,cpf,senha,is_active,created_at,created_by,updated_at,updated_by,}: UserByCpfBorrowed<'a>) ->
    Self { Self { id,nome: nome.into(),email: email.into(),cpf: cpf.into(),senha: senha.into(),is_active: is_active.into(),created_at,created_by,updated_at,updated_by,} }
}pub struct UserByCpfQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> UserByCpfBorrowed,
    mapper: fn(UserByCpfBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> UserByCpfQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(UserByCpfBorrowed) -> R) ->
    UserByCpfQuery<'a,C,R,N>
    {
        UserByCpfQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct UserByEmail
{ pub id : i32,pub nome : String,pub email : String,pub cpf : String,pub senha : String,pub is_active : String,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}pub struct UserByEmailBorrowed<'a> { pub id : i32,pub nome : &'a str,pub email : &'a str,pub cpf : &'a str,pub senha : &'a str,pub is_active : &'a str,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}
impl<'a> From<UserByEmailBorrowed<'a>> for UserByEmail
{
    fn from(UserByEmailBorrowed { id,nome,email,cpf,senha,is_active,created_at,created_by,updated_at,updated_by,}: UserByEmailBorrowed<'a>) ->
    Self { Self { id,nome: nome.into(),email: email.into(),cpf: cpf.into(),senha: senha.into(),is_active: is_active.into(),created_at,created_by,updated_at,updated_by,} }
}pub struct UserByEmailQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> UserByEmailBorrowed,
    mapper: fn(UserByEmailBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> UserByEmailQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(UserByEmailBorrowed) -> R) ->
    UserByEmailQuery<'a,C,R,N>
    {
        UserByEmailQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct AtualizaUsuario
{ pub id : i32,pub nome : String,pub email : String,pub cpf : String,pub senha : String,pub is_active : String,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}pub struct AtualizaUsuarioBorrowed<'a> { pub id : i32,pub nome : &'a str,pub email : &'a str,pub cpf : &'a str,pub senha : &'a str,pub is_active : &'a str,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}
impl<'a> From<AtualizaUsuarioBorrowed<'a>> for AtualizaUsuario
{
    fn from(AtualizaUsuarioBorrowed { id,nome,email,cpf,senha,is_active,created_at,created_by,updated_at,updated_by,}: AtualizaUsuarioBorrowed<'a>) ->
    Self { Self { id,nome: nome.into(),email: email.into(),cpf: cpf.into(),senha: senha.into(),is_active: is_active.into(),created_at,created_by,updated_at,updated_by,} }
}pub struct AtualizaUsuarioQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> AtualizaUsuarioBorrowed,
    mapper: fn(AtualizaUsuarioBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> AtualizaUsuarioQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(AtualizaUsuarioBorrowed) -> R) ->
    AtualizaUsuarioQuery<'a,C,R,N>
    {
        AtualizaUsuarioQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}#[derive(serde::Serialize, Debug, Clone, PartialEq,)] pub struct TodosUsuarios
{ pub id : i32,pub nome : String,pub email : String,pub cpf : String,pub senha : String,pub is_active : String,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}pub struct TodosUsuariosBorrowed<'a> { pub id : i32,pub nome : &'a str,pub email : &'a str,pub cpf : &'a str,pub senha : &'a str,pub is_active : &'a str,pub created_at : time::PrimitiveDateTime,pub created_by : i32,pub updated_at : time::PrimitiveDateTime,pub updated_by : i32,}
impl<'a> From<TodosUsuariosBorrowed<'a>> for TodosUsuarios
{
    fn from(TodosUsuariosBorrowed { id,nome,email,cpf,senha,is_active,created_at,created_by,updated_at,updated_by,}: TodosUsuariosBorrowed<'a>) ->
    Self { Self { id,nome: nome.into(),email: email.into(),cpf: cpf.into(),senha: senha.into(),is_active: is_active.into(),created_at,created_by,updated_at,updated_by,} }
}pub struct TodosUsuariosQuery<'a, C: GenericClient, T, const N: usize>
{
    client: &'a  C, params:
    [&'a (dyn postgres_types::ToSql + Sync); N], stmt: &'a mut
    cornucopia_async::private::Stmt, extractor: fn(&tokio_postgres::Row) -> TodosUsuariosBorrowed,
    mapper: fn(TodosUsuariosBorrowed) -> T,
} impl<'a, C, T:'a, const N: usize> TodosUsuariosQuery<'a, C, T, N> where C:
GenericClient
{
    pub fn map<R>(self, mapper: fn(TodosUsuariosBorrowed) -> R) ->
    TodosUsuariosQuery<'a,C,R,N>
    {
        TodosUsuariosQuery
        {
            client: self.client, params: self.params, stmt: self.stmt,
            extractor: self.extractor, mapper,
        }
    } pub async fn one(self) -> Result<T, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let row =
        self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    } pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error>
    { self.iter().await?.try_collect().await } pub async fn opt(self) ->
    Result<Option<T>, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self.client.query_opt(stmt, &self.params) .await?
        .map(|row| (self.mapper)((self.extractor)(&row))))
    } pub async fn iter(self,) -> Result<impl futures::Stream<Item = Result<T,
    tokio_postgres::Error>> + 'a, tokio_postgres::Error>
    {
        let stmt = self.stmt.prepare(self.client).await?; let it =
        self.client.query_raw(stmt,
        cornucopia_async::private::slice_iter(&self.params)) .await?
        .map(move |res|
        res.map(|row| (self.mapper)((self.extractor)(&row)))) .into_stream();
        Ok(it)
    }
}pub fn cadastra_usuario() -> CadastraUsuarioStmt
{ CadastraUsuarioStmt(cornucopia_async::private::Stmt::new("INSERT into usuario (
    nome, email, cpf, senha
)
VALUES (
    $1, $2, $3, $4
)
returning *")) } pub struct
CadastraUsuarioStmt(cornucopia_async::private::Stmt); impl CadastraUsuarioStmt
{ pub fn bind<'a, C:
GenericClient,T1:
cornucopia_async::StringSql,T2:
cornucopia_async::StringSql,T3:
cornucopia_async::StringSql,T4:
cornucopia_async::StringSql,>(&'a mut self, client: &'a  C,
nome: &'a T1,email: &'a T2,cpf: &'a T3,senha: &'a T4,) -> CadastraUsuarioQuery<'a,C,
CadastraUsuario, 4>
{
    CadastraUsuarioQuery
    {
        client, params: [nome,email,cpf,senha,], stmt: &mut self.0, extractor:
        |row| { CadastraUsuarioBorrowed { id: row.get(0),nome: row.get(1),email: row.get(2),cpf: row.get(3),senha: row.get(4),is_active: row.get(5),created_at: row.get(6),created_by: row.get(7),updated_at: row.get(8),updated_by: row.get(9),} }, mapper: |it| { <CadastraUsuario>::from(it) },
    }
} }impl <'a, C: GenericClient,T1: cornucopia_async::StringSql,T2: cornucopia_async::StringSql,T3: cornucopia_async::StringSql,T4: cornucopia_async::StringSql,> cornucopia_async::Params<'a,
CadastraUsuarioParams<T1,T2,T3,T4,>, CadastraUsuarioQuery<'a, C,
CadastraUsuario, 4>, C> for CadastraUsuarioStmt
{
    fn
    params(&'a mut self, client: &'a  C, params: &'a
    CadastraUsuarioParams<T1,T2,T3,T4,>) -> CadastraUsuarioQuery<'a, C,
    CadastraUsuario, 4>
    { self.bind(client, &params.nome,&params.email,&params.cpf,&params.senha,) }
}pub fn login_usuario() -> LoginUsuarioStmt
{ LoginUsuarioStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM usuario 
WHERE cpf = $1")) } pub struct
LoginUsuarioStmt(cornucopia_async::private::Stmt); impl LoginUsuarioStmt
{ pub fn bind<'a, C:
GenericClient,T1:
cornucopia_async::StringSql,>(&'a mut self, client: &'a  C,
cpf: &'a T1,) -> LoginUsuarioQuery<'a,C,
LoginUsuario, 1>
{
    LoginUsuarioQuery
    {
        client, params: [cpf,], stmt: &mut self.0, extractor:
        |row| { LoginUsuarioBorrowed { id: row.get(0),nome: row.get(1),email: row.get(2),cpf: row.get(3),senha: row.get(4),is_active: row.get(5),created_at: row.get(6),created_by: row.get(7),updated_at: row.get(8),updated_by: row.get(9),} }, mapper: |it| { <LoginUsuario>::from(it) },
    }
} }pub fn user_by_id() -> UserByIdStmt
{ UserByIdStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM usuario 
WHERE id = $1")) } pub struct
UserByIdStmt(cornucopia_async::private::Stmt); impl UserByIdStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
id: &'a i32,) -> UserByIdQuery<'a,C,
UserById, 1>
{
    UserByIdQuery
    {
        client, params: [id,], stmt: &mut self.0, extractor:
        |row| { UserByIdBorrowed { id: row.get(0),nome: row.get(1),email: row.get(2),cpf: row.get(3),senha: row.get(4),is_active: row.get(5),created_at: row.get(6),created_by: row.get(7),updated_at: row.get(8),updated_by: row.get(9),} }, mapper: |it| { <UserById>::from(it) },
    }
} }pub fn user_by_cpf() -> UserByCpfStmt
{ UserByCpfStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM usuario 
WHERE cpf = $1")) } pub struct
UserByCpfStmt(cornucopia_async::private::Stmt); impl UserByCpfStmt
{ pub fn bind<'a, C:
GenericClient,T1:
cornucopia_async::StringSql,>(&'a mut self, client: &'a  C,
cpf: &'a T1,) -> UserByCpfQuery<'a,C,
UserByCpf, 1>
{
    UserByCpfQuery
    {
        client, params: [cpf,], stmt: &mut self.0, extractor:
        |row| { UserByCpfBorrowed { id: row.get(0),nome: row.get(1),email: row.get(2),cpf: row.get(3),senha: row.get(4),is_active: row.get(5),created_at: row.get(6),created_by: row.get(7),updated_at: row.get(8),updated_by: row.get(9),} }, mapper: |it| { <UserByCpf>::from(it) },
    }
} }pub fn user_by_email() -> UserByEmailStmt
{ UserByEmailStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM usuario 
WHERE email = $1")) } pub struct
UserByEmailStmt(cornucopia_async::private::Stmt); impl UserByEmailStmt
{ pub fn bind<'a, C:
GenericClient,T1:
cornucopia_async::StringSql,>(&'a mut self, client: &'a  C,
email: &'a T1,) -> UserByEmailQuery<'a,C,
UserByEmail, 1>
{
    UserByEmailQuery
    {
        client, params: [email,], stmt: &mut self.0, extractor:
        |row| { UserByEmailBorrowed { id: row.get(0),nome: row.get(1),email: row.get(2),cpf: row.get(3),senha: row.get(4),is_active: row.get(5),created_at: row.get(6),created_by: row.get(7),updated_at: row.get(8),updated_by: row.get(9),} }, mapper: |it| { <UserByEmail>::from(it) },
    }
} }pub fn atualiza_usuario() -> AtualizaUsuarioStmt
{ AtualizaUsuarioStmt(cornucopia_async::private::Stmt::new("UPDATE usuario
SET nome = $1, email = $2
WHERE id = $3
returning *")) } pub struct
AtualizaUsuarioStmt(cornucopia_async::private::Stmt); impl AtualizaUsuarioStmt
{ pub fn bind<'a, C:
GenericClient,T1:
cornucopia_async::StringSql,T2:
cornucopia_async::StringSql,>(&'a mut self, client: &'a  C,
nome: &'a T1,email: &'a T2,id: &'a i32,) -> AtualizaUsuarioQuery<'a,C,
AtualizaUsuario, 3>
{
    AtualizaUsuarioQuery
    {
        client, params: [nome,email,id,], stmt: &mut self.0, extractor:
        |row| { AtualizaUsuarioBorrowed { id: row.get(0),nome: row.get(1),email: row.get(2),cpf: row.get(3),senha: row.get(4),is_active: row.get(5),created_at: row.get(6),created_by: row.get(7),updated_at: row.get(8),updated_by: row.get(9),} }, mapper: |it| { <AtualizaUsuario>::from(it) },
    }
} }impl <'a, C: GenericClient,T1: cornucopia_async::StringSql,T2: cornucopia_async::StringSql,> cornucopia_async::Params<'a,
AtualizaUsuarioParams<T1,T2,>, AtualizaUsuarioQuery<'a, C,
AtualizaUsuario, 3>, C> for AtualizaUsuarioStmt
{
    fn
    params(&'a mut self, client: &'a  C, params: &'a
    AtualizaUsuarioParams<T1,T2,>) -> AtualizaUsuarioQuery<'a, C,
    AtualizaUsuario, 3>
    { self.bind(client, &params.nome,&params.email,&params.id,) }
}pub fn todos_usuarios() -> TodosUsuariosStmt
{ TodosUsuariosStmt(cornucopia_async::private::Stmt::new("SELECT *
FROM usuario
ORDER BY id")) } pub struct
TodosUsuariosStmt(cornucopia_async::private::Stmt); impl TodosUsuariosStmt
{ pub fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
) -> TodosUsuariosQuery<'a,C,
TodosUsuarios, 0>
{
    TodosUsuariosQuery
    {
        client, params: [], stmt: &mut self.0, extractor:
        |row| { TodosUsuariosBorrowed { id: row.get(0),nome: row.get(1),email: row.get(2),cpf: row.get(3),senha: row.get(4),is_active: row.get(5),created_at: row.get(6),created_by: row.get(7),updated_at: row.get(8),updated_by: row.get(9),} }, mapper: |it| { <TodosUsuarios>::from(it) },
    }
} }pub fn excluir_usuario() -> ExcluirUsuarioStmt
{ ExcluirUsuarioStmt(cornucopia_async::private::Stmt::new("DELETE
FROM usuario
WHERE id = $1")) } pub struct
ExcluirUsuarioStmt(cornucopia_async::private::Stmt); impl ExcluirUsuarioStmt
{ pub async fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
id: &'a i32,) -> Result<u64, tokio_postgres::Error>
{
    let stmt = self.0.prepare(client).await?;
    client.execute(stmt, &[id,]).await
} }pub fn inativar_usuario() -> InativarUsuarioStmt
{ InativarUsuarioStmt(cornucopia_async::private::Stmt::new("UPDATE usuario
SET is_active = 'F'
WHERE id = $1")) } pub struct
InativarUsuarioStmt(cornucopia_async::private::Stmt); impl InativarUsuarioStmt
{ pub async fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
id: &'a i32,) -> Result<u64, tokio_postgres::Error>
{
    let stmt = self.0.prepare(client).await?;
    client.execute(stmt, &[id,]).await
} }pub fn ativar_usuario() -> AtivarUsuarioStmt
{ AtivarUsuarioStmt(cornucopia_async::private::Stmt::new("UPDATE usuario
SET is_active = 'T'
WHERE id = $1")) } pub struct
AtivarUsuarioStmt(cornucopia_async::private::Stmt); impl AtivarUsuarioStmt
{ pub async fn bind<'a, C:
GenericClient,>(&'a mut self, client: &'a  C,
id: &'a i32,) -> Result<u64, tokio_postgres::Error>
{
    let stmt = self.0.prepare(client).await?;
    client.execute(stmt, &[id,]).await
} }}}