use deadpool_postgres::{Config, CreatePoolError, Runtime};
use tokio_postgres::NoTls;

use dotenv::dotenv;

pub async fn create_pool() -> Result<deadpool_postgres::Pool, CreatePoolError> {
    let mut cfg = Config::new();
    dotenv().ok();
    cfg.user = Some(
        std::env::var("DB_USER")
            .expect("DB_USER not set in .env file")
            .parse()
            .unwrap(),
    );
    cfg.password = Some(
        std::env::var("DB_PASSWORD")
            .expect("DB_PASSWORD not set in .env file")
            .parse()
            .unwrap(),
    );
    cfg.host = Some(
        std::env::var("DB_HOST")
            .expect("DB_HOST not set in .env file")
            .parse()
            .unwrap(),
    );
    cfg.port = Some(
        std::env::var("DB_PORT")
            .expect("DB_PORT not set in .env file")
            .parse()
            .unwrap(),
    );
    cfg.dbname = Some(
        std::env::var("DB_NAME")
            .expect("DB_NAME not set in .env file")
            .parse()
            .unwrap(),
    );
    cfg.create_pool(Some(Runtime::Tokio1), NoTls)
}
 