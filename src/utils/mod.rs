pub mod db;
pub mod templates;

pub struct AppState {
    pub pool: deadpool_postgres::Pool,
}

pub fn cpf_clean(cpf: &str) -> String {
    cpf.replace(['.', '-'], "")
}
pub fn cpf_format(cpf: &str) -> String {
    let cpf = cpf.replace(['.', '-'], "");
    let cpf = cpf.chars().collect::<Vec<char>>();
    format!(
        "{}.{}.{}-{}",
        cpf[0..3].iter().collect::<String>(),
        cpf[3..6].iter().collect::<String>(),
        cpf[6..9].iter().collect::<String>(),
        cpf[9..11].iter().collect::<String>()
    )
}