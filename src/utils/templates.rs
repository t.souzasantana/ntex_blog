use ntex::web;
//add minijinjua stuff
use minijinja::{self,Value};
use once_cell::sync::Lazy;

pub fn render_minijinja(template: &str, ctx: Value) -> Result<web::HttpResponse, ntex::web::Error> {

    let tmpl = match TEMPLATE.get_template(template) {
        Ok(tmpl) => tmpl,
        Err(error) => {
        //println!("Minijinja error: {:#?}", error);
            return Ok(web::HttpResponse::Ok()
                .content_type("text/html")
                .body(error.to_string()));
        }
    };

    let rendered = tmpl.render(ctx);

    match rendered {
        Ok(result) => Ok(web::HttpResponse::Ok().content_type("text/html").body(result)),
        // Err(error) => Ok(HttpResponse::Ok().content_type("text/html").body(error.to_string())),
        Err(error) => {
            let cause = error;
            ////println!("Minijinja error: {:#?}", cause);

            Ok(web::HttpResponse::Ok()
                .content_type("text/html")
                .body(cause.to_string()))
        }
    }
}

static TEMPLATE: Lazy<minijinja::Environment<'static>> = Lazy::new(|| {
    let mut env = minijinja::Environment::new();
    minijinja_contrib::add_to_environment(&mut env);
    let tmpl_path = concat!(env!("CARGO_MANIFEST_DIR"), "/templates");
    env.set_loader(minijinja::path_loader(tmpl_path));
    env
});
