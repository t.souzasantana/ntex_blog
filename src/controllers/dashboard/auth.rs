use ntex::channel::pool::new;
use ntex::http::h1::Payload;
use ntex::web::{self, Responder};
//add minijinjua stuff
use crate::cornucopia::{self, queries::usuario, queries::grupos, queries::funcionalidade, queries::perfil, queries::permissao};
use crate::utils::cpf_format;
use crate::utils::db::create_pool;
use crate::utils::templates::render_minijinja;
use cpf_cnpj::cpf;
use djangohashers::*;
use minijinja::{self, context};
use ntex::http::header;
use ntex_session::Session;
use serde::{Deserialize, Serialize};
// use serde_json::{Result, Value};
// use validator::{Validate, ValidationError};

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(signup)
        .service(signin)
        .service(update_signup)
        .service(do_signin)
        .service(profile)
        .service(update_profile)
        .service(usuarios)
        .service(edit_usuario)
        .service(update_usuario)
        .service(inativa_usuario)
        .service(ativa_usuario)
        .service(lista_funcionalidades)
        .service(update_funcionalidade)
        // .service(delete_funcionalidade)
        .service(lista_perfis)
        .service(edit_perfil)
        // .service(update_perfil)
        // .service(delete_perfil)
        .service(lista_grupos)
        // .service(update_grupo)
        // .service(delete_grupo)
        .service(add_grupo)
        .service(lista_permissoes)
        .service(edit_permissao)
        .service(update_permissao)
        // .service(delete_permissao)
        ;
}

#[web::get("/dashboard/auth/signup")]
async fn signup(
    session: Session,
    // data: web::types::State<AppState>,
) -> impl Responder {
    //pega da sessão o número de vezes que a página foi visitada e adiciona mais um
    let visits = session.get::<i32>("visits").unwrap_or(Some(0)).unwrap_or(0) + 1;
    //armazena na sessão a contagem de visitas
    session.set("visits", visits).unwrap();
    let pool = create_pool().await.unwrap();
    let _client = pool.get().await.unwrap();
    // let usuario = crate::cornucopia::queries::usuario::login_usuario()
    // .bind(&client, &"11111111111")
    // .opt()
    // .await
    // .unwrap();
    let session_errors = &session.get::<ErrorsSignup>("form_errors").unwrap();
    let e = &ErrorsSignup::default();
    let form_errors = match session_errors {
        Some(session_errors) => session_errors,
        None => e,
    };
    let f = &FormSignup::default();
    let sessionform = &session.get::<FormSignup>("form_signup").unwrap();
    let form: &FormSignup = match sessionform {
        Some(sessionform) => sessionform,
        None => f,
    };
    //let flash="";
    //let msg_error="";


    #[derive(Serialize, Deserialize)]
    struct Author {
        name: String,
        email: String,
        date: String,
    }

    let author = Author {
        name: "Thiago".to_string(),
        email: "t.souzasantana@gmail.com".to_string(),
        date: "20 de junho de 2024".to_string(),
    };
    let usuario: AuthUser = AuthUser::default();
    let flash = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    session.remove("flash");
    session.remove("msg_error");
    let ctx = context!(flash, msg_error, author, visits, usuario, form, form_errors);
    render_minijinja("dashboard/auth/signup.html", ctx)
}

#[web::get("/dashboard/auth/signin")]
async fn signin(
    session: Session,
    // data: web::types::State<AppState>,
) -> impl Responder {
    //pega da sessão o número de vezes que a página foi visitada e adiciona mais um
    let visits = session.get::<i32>("visits").unwrap_or(Some(0)).unwrap_or(0) + 1;
    //armazena na sessão a contagem de visitas
    // session.set("visits", visits).unwrap();
    let pool = create_pool().await.unwrap();
    let _client = pool.get().await.unwrap();
    // let usuario = crate::cornucopia::queries::usuario::login_usuario()
    // .bind(&client, &"11111111111", &"esportes")
    // .opt()
    // .await
    // .unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());

    #[derive(Serialize, Deserialize)]
    struct Author {
        name: String,
        email: String,
        date: String,
    }

    let author = Author {
        name: "Thiago".to_string(),
        email: "t.souzasantana@gmail.com".to_string(),
        date: "20 de junho de 2024".to_string(),
    };
    let usuario = AuthUser::default();
    let ctx = context!(flash, msg_error, author, visits, usuario);
    render_minijinja("dashboard/auth/signin.html", ctx)
}

#[derive(Serialize, Deserialize, Default)]
struct FormSignup {
    nome: String,
    cpf: String,
    email: String,
    senha: String,
    conferesenha: String,
}

#[derive(Serialize, Deserialize, Default)]
struct AddGrupo {
    usuario_id: i32,
    perfil_id: i32,
}

#[derive(Serialize, Deserialize, Default)]
struct FormProfile {
    nome: String,
    email: String,
}

#[derive(Serialize, Deserialize, Default)]
struct FormEditUsuario {
    id: i32,
    nome: String,
    email: String,
}

#[derive(Serialize, Deserialize, Default)]
struct FormEditFuncionalidade {
    id: i32,
    descricao: String,
    funcionalidade: String,
}

#[derive(Serialize, Deserialize, Default)]
struct ErrorsSignup {
    nome: String,
    cpf: String,
    email: String,
    senha: String,
    conferesenha: String,
}

#[web::post("/dashboard/auth/signup")]
async fn update_signup(
    form: web::types::Form<FormSignup>,
    session: Session,
    // data: web::types::State<AppState>,
) -> impl Responder {
    let form: FormSignup = form.into_inner();
    
    //inicio tratamento de erro
    let mut errors = ErrorsSignup::default();
    let mut count_errors = 0;
    dbg!(&count_errors);
    session.set("form_signup", &form).unwrap();
    let pool = create_pool().await.unwrap();
    let client = pool.get().await.unwrap();
    let nome = match &form.nome.len() > &5 {
        true => &form.nome,
        false => {
            errors.nome = "O campo nome deve ser preenchido com no mínimo 6 caracteres".to_string();
            count_errors += 1;
            &form.nome
            // session.set("msg_error", "O campo nome deve ser preenchido com no mínimo 6 caracteres").unwrap();

            // return web::HttpResponse::Found()
            // .set_header(header::LOCATION, "/dashboard/auth/signup")
            // .finish();
        }
    };

    let email = match &form.email.contains("@") & &form.email.contains(".") {
        true => {
            let checkuser = cornucopia::queries::usuario::user_by_email()
                .bind(&client, &form.email)
                .opt()
                .await
                .unwrap();

            match checkuser {
                Some(row) => {
                    errors.email = "E-mail já cadastrado.".to_string();
                    count_errors += 1;
                    
                    // session.set("msg_error", "E-mail já cadastrado.").unwrap();
                    // return web::HttpResponse::Found()
                    // .set_header(header::LOCATION, "/dashboard/auth/signup")
                    // .finish();
                }

                None => {}
            }

            &form.email
        }
        false => {
            errors.email = "E-mail inválido.".to_string();
            count_errors += 1;
            &form.email
            // session.set("msg_error", "E-mail inválido").unwrap();

            // return web::HttpResponse::Found()
            // .set_header(header::LOCATION, "/dashboard/auth/signup")
            // .finish();
        }
    };


    let senha_criptografada = match &form.senha == &form.conferesenha {
        true => make_password(&form.senha),
        false => {
            errors.conferesenha = "As senhas não conferem. Cadastre novamente.".to_string();
            count_errors += 1;
            make_password(&form.senha)
            // session.set("msg_error", "As senhas não conferem. Cadastre novamente.").unwrap();

            // return web::HttpResponse::Found()
            // .set_header(header::LOCATION, "/dashboard/auth/signup")
            // .finish();
        }
    };

    let cpf_validado = match cpf::validate(&form.cpf) {
        true => {
            let cpf = crate::utils::cpf_clean(&form.cpf);
            
            let checkuser = cornucopia::queries::usuario::user_by_cpf()
                .bind(&client, &cpf)
                .opt()
                .await
                .unwrap();

            match checkuser {
                Some(row) => {
                    errors.cpf = "CPF já cadastrado. Use a recuperação de senha.".to_string();
                    // session
                    //     .set(
                    //         "msg_error",
                    //         "CPF já cadastrado, use a recuperação de conta.",
                    //     )
                    //     .unwrap();
                    // return web::HttpResponse::Found()
                    //     .set_header(header::LOCATION, "/dashboard/auth/signin")
                    //     .finish();
                }

                None => {}
            }

            cpf_format(&form.cpf)
        }
        false => {
            errors.cpf = "CPF não válido. Cadastre novamente.".to_string();
            count_errors += 1;
            cpf_format(&form.cpf)
            // session.set("msg_error", "CPF não válido. Cadastre novamente.").unwrap();

            // return web::HttpResponse::Found()
            // .set_header(header::LOCATION, "/dashboard/auth/signup")
            // .finish();
        }
    };

    if count_errors == 0 {
        let user: cornucopia::queries::usuario::CadastraUsuario = cornucopia::queries::usuario::cadastra_usuario()
                .bind(&client, &nome, &email, &cpf_validado, &senha_criptografada)
                .opt()
                .await
                .unwrap()
                .unwrap();

        // TODO: enviar e-mail para validação
        session
            .set(
                "flash",
                "Seu cadastro foi feito com sucesso. Você recebeu o e-mail. Clique para validar.",
            )
            .unwrap();
        
        return web::HttpResponse::Found()
            .set_header(header::LOCATION, "/dashboard/auth/signin")
            .finish();
    } else {
        session.set("form_errors", errors).unwrap();
        
        return web::HttpResponse::Found()
            .set_header(header::LOCATION, "/dashboard/auth/signin")
            .finish();
    }
    //final tratamento de erro
}

#[derive(Serialize, Deserialize, Default)]
struct FormSignin {
    cpf: String,
    senha: String,
}

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct AuthUser {
    id: i32,
    nome: String,
    email: String,
    cpf: String,
    is_active: String,
}

#[web::post("/dashboard/auth/signin")]
async fn do_signin(
    form: web::types::Form<FormSignin>,
    session: Session,
    // data: web::types::State<AppState>,
) -> impl Responder {
    let form = form.into_inner();
    let pool = create_pool().await.unwrap();
    let client = pool.get().await.unwrap();
    
    let row_user = cornucopia::queries::usuario::login_usuario()
        .bind(&client, &cpf_format(&form.cpf))
        .opt()
        .await
        .unwrap();

    let user = match row_user {
        Some(row_user) => row_user,
        None => {
            session.set("msg_error", "Usuário inexistente.").unwrap();
            return web::HttpResponse::Found()
                .set_header(header::LOCATION, "/dashboard/auth/signin")
                .finish();
        }
    };

    let senha_criptografada: String = make_password(&form.senha);
    let is_valid = check_password(&form.senha, &user.senha).unwrap();
    println!("Is valid: {:?}", is_valid);
    match is_valid {
        true => {}

        false => {
            session.set("msg_error", "Senha incorreta.").unwrap();
            return web::HttpResponse::Found()
                .set_header(header::LOCATION, "/dashboard/auth/signin")
                .finish();
        }
    }
    let usuario: AuthUser = AuthUser {
        id: user.id,
        nome: user.nome,
        email: user.email,
        cpf: user.cpf,
        is_active: user.is_active,
    };
    session.set("auth_user", usuario).unwrap();

    web::HttpResponse::Found()
        .set_header(header::LOCATION, "/dashboard/auth/profile")
        .finish()
}

#[web::get("/dashboard/auth/profile")]
async fn profile(session: Session) -> impl Responder {
    //pega da sessão os dados do usuário, se logado
    let pool = create_pool().await.unwrap();
    let client = pool.get().await.unwrap();
    let auth_user = match session.get::<AuthUser>("auth_user") {
        Ok(value) => value.unwrap(),
        Err(_) => AuthUser::default(),
    };
    let form_errors = ErrorsSignup::default();
    dbg!(&auth_user);
    let form = cornucopia::queries::usuario::user_by_id()
        .bind(&client, &auth_user.id)
        .opt()
        .await
        .unwrap()
        .unwrap();
    let flash = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    session.remove("flash");
    session.remove("msg_error");
    let ctx = context!(flash, msg_error, form, form_errors, auth_user);
    render_minijinja("dashboard/auth/profile.html", ctx)
    // if user_id == 0 {
    //     web::HttpResponse::Found()
    //     .set_header(header::LOCATION, "/dashboard/auth/signin")
    //     .finish()
    // } else {
    //     let user = cornucopia::queries::usuario::user_by_id()
    //     .bind(&client,
    //         &user_id)
    //     .opt()
    //     .await
    //     .unwrap()
    //     .unwrap();
    //     //mensagem em caso de sucesso
    //     let flash = "Está dando tudo certo";
    //     //mensagem em caso de erro
    //     let msg_error = "Deu algum problema";
    //     let ctx = context!(flash, msg_error, user);
    //     render_minijinja("dashboard/auth/signup.html", ctx)
    // }
}



#[web::post("/dashboard/auth/profile")]
async fn update_profile(
    form: web::types::Form<FormProfile>,
    session: Session,
) -> impl Responder {
    let form = form.into_inner();
    let auth_user = match session.get::<AuthUser>("auth_user") {
        Ok(value) => {value.unwrap()},
        Err(_) => AuthUser::default(),
    };
    //inicio tratamento de erro
    let mut errors = ErrorsSignup::default();
    let mut count_errors = 0;
    dbg!(&count_errors);
    session.set("form_signup", &form).unwrap();
    let pool = create_pool().await.unwrap();
    let client = pool.get().await.unwrap();
    let nome = match &form.nome.len() > &5 {
        true => &form.nome,
        false => {
            errors.nome = "O nome deve ter mais de 5 caracteres.".to_string();
            count_errors += 1;
            dbg!(&count_errors);
            &form.nome
            //     session.set("msg_error", "O nome deve ter mais de 5 caracteres.").unwrap();

            //     return web::HttpResponse::Found()
            //     .set_header(header::LOCATION, "/dashboard/auth/signup")
            //     .finish();
        }
    };

    let email = match &form.email.contains("@") & &form.email.contains(".") {
        true => {
            let checkuser = cornucopia::queries::usuario::user_by_email()
                .bind(&client, &form.email)
                .opt()
                .await
                .unwrap();

            match checkuser {
                Some(row) => {
                    if &row.id != &auth_user.id {
                        errors.email = "Email já cadastrado em outro CPF.".to_string();
                        count_errors += 1;
                        dbg!(&count_errors);
                        // session.set("msg_error", "Email já cadastrado em outro CPF.").unwrap();
                        // return web::HttpResponse::Found()
                        // .set_header(header::LOCATION, "/dashboard/auth/signup")
                        // .finish();
                    }
                }

                None => {}
            }

            &form.email
        }
        false => {
            errors.email = "Email invalido.".to_string();
            count_errors += 1;
            dbg!(&count_errors);
            &form.email
            // session.set("msg_error", "email invalido").unwrap();

            // return web::HttpResponse::Found()
            // .set_header(header::LOCATION, "/dashboard/auth/signup")
            // .finish();
        }
    };
    dbg!(&count_errors);
    if count_errors == 0 {
        println!("valores indo para o banco {} {} {}", &nome, &email, &auth_user.id
        );
        let user = cornucopia::queries::usuario::atualiza_usuario()
            .bind(&client, &nome, &email, &auth_user.id)
            .opt()
            .await
            .unwrap()
            .unwrap();
        dbg!(user);
        // TODO: enviar e-mail para validação
        session
            .set("flash", "Seu perfil foi atualizado com sucesso.")
            .unwrap();

        return web::HttpResponse::Found()
            .set_header(header::LOCATION, "/dashboard/auth/profile")
            .finish();
    } else {
        session.set("form_errors", errors).unwrap();

        return web::HttpResponse::Found()
            .set_header(header::LOCATION, "/dashboard/auth/profile")
            .finish();
    }
    //final tratamento de erro
}

#[web::get("/dashboard/auth/usuarios")]
async fn usuarios(
    session: Session,
) -> impl Responder {
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    // let usuario = crate::cornucopia::queries::usuario::login_usuario()
    // .bind(&client, &"11111111111", &"esportes")
    // .opt()
    // .await
    // .unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let usuarios = usuario::todos_usuarios()
        .bind(&conexao)
        .all()
        .await
        .unwrap();
    
    let ctx = context!(flash, msg_error, usuarios);
    render_minijinja("dashboard/auth/usuarios.html", ctx)
}


#[web::get("/dashboard/auth/edit_usuario/{usuario_id}")]
async fn edit_usuario(
    session: Session,
    path: web::types::Path<(i32,)>,
) -> impl Responder {
    let path = path.into_inner();
    println!("{}", path.0);
    let usuario_id = path.0;
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let form_errors = ErrorsSignup::default();
    let usuario = usuario::user_by_id()
        .bind(&conexao, &usuario_id)
        .one()
        .await
        .unwrap();
    session.remove("flash");
    session.remove("msg_error");
    let ctx = context!(flash, msg_error, form_errors, usuario);
    render_minijinja("dashboard/auth/edit_usuario.html", ctx)
}

#[web::post("/dashboard/auth/edit_usuario")]
async fn update_usuario(
    form: web::types::Form<FormEditUsuario>,
    session: Session,
) -> impl Responder {
    let form = form.into_inner();
    //inicio tratamento de erro
    // let mut errors: ErrorsSignup = ErrorsSignup::default();
    // let mut count_errors = 0;
    // dbg!(&count_errors);
    session.set("form_signup", &form).unwrap();
    let pool = create_pool().await.unwrap();
    let client = pool.get().await.unwrap();
    let id = &form.id;
    let nome: &String =  &form.nome;
        // let nome: &String = match &form.nome.len() > &5 {
        // true => &form.nome,
        // false => {
        //     errors.nome = "O nome deve ter mais de 5 caracteres.".to_string();
        //     count_errors += 1;
        //     dbg!(&count_errors);
        //     &form.nome
        // }
    // };

    let email =  &form.email;
    // let email = match &form.email.contains("@") & &form.email.contains(".") {
    //     true => {
    //         let checkuser = cornucopia::queries::usuario::user_by_email()
    //             .bind(&client, &form.email)
    //             .opt()
    //             .await
    //             .unwrap();

    //         match checkuser {
    //             Some(row) => {
    //                 if &row.id != id {
    //                     errors.email = "Email já cadastrado em outro CPF.".to_string();
    //                     count_errors += 1;
    //                     dbg!(&count_errors);
    //                 }
    //             }

    //             None => {}
    //         }

    //         &form.email
    //     }
    //     false => {
    //         errors.email = "Email invalido.".to_string();
    //         count_errors += 1;
    //         dbg!(&count_errors);
    //         &form.email
    //     }
    // };
    // dbg!(&count_errors);
    // if count_errors == 0 {
    //     println!("valores indo para o banco {} {} {}", &nome, &email, &id
    //     );
        let user: usuario::AtualizaUsuario = cornucopia::queries::usuario::atualiza_usuario()
            .bind(&client, &nome, &email, &id)
            .opt()
            .await
            .unwrap()
            .unwrap();
        dbg!(user);
        session
            .set("flash", "Usuário atualizado com sucesso.")
            .unwrap();

        return web::HttpResponse::Found()
            .set_header(header::LOCATION, "/dashboard/auth/usuarios")
            .finish();
    // } else {
    //     session.set("form_errors", errors).unwrap();

    //     return web::HttpResponse::Found()
    //         .set_header(header::LOCATION, "/dashboard/auth/edit_usuario")
    //         .finish();
    // }
    //final tratamento de erro
}

//DÚVIDAS: verificar como corrigir direcionamento
#[web::get("/dashboard/auth/inativa_usuario/{usuario_id}")]
async fn inativa_usuario(
    session: Session,
    path: web::types::Path<(i32,)>,
) -> impl Responder {
    let path = path.into_inner();
    println!("{}", path.0);
    let id = path.0;
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let form_errors = ErrorsSignup::default();

    let inativar_usuario = usuario::inativar_usuario()
        .bind(&conexao, &id)
        .await
        .unwrap();

    session.remove("flash");
    session.remove("msg_error");

    let ctx = context!(flash, msg_error, form_errors, inativar_usuario);
    render_minijinja("dashboard/auth/usuarios.html", ctx);

    return web::HttpResponse::Found()
    .set_header(header::LOCATION, "/dashboard/auth/usuarios")
    .finish();

}

#[web::get("/dashboard/auth/ativa_usuario/{usuario_id}")]
async fn ativa_usuario(
    session: Session,
    path: web::types::Path<(i32,)>,
) -> impl Responder {
    let path = path.into_inner();
    println!("{}", path.0);
    let id = path.0;
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let form_errors = ErrorsSignup::default();

    let ativar_usuario = usuario::ativar_usuario()
        .bind(&conexao, &id)
        .await
        .unwrap();

    session.remove("flash");
    session.remove("msg_error");

    let ctx = context!(flash, msg_error, form_errors, ativar_usuario);
    render_minijinja("dashboard/auth/usuarios.html", ctx);

    return web::HttpResponse::Found()
    .set_header(header::LOCATION, "/dashboard/auth/usuarios")
    .finish();

}


#[web::get("/dashboard/auth/lista_grupos")]
async fn lista_grupos(
    session: Session,
) -> impl Responder {
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let grupos = grupos::todos_grupos()
        .bind(&conexao)
        .all()
        .await
        .unwrap();
    
    let ctx = context!(flash, msg_error, grupos);
    render_minijinja("dashboard/auth/lista_grupos.html", ctx)
}

#[web::post("/dashboard/auth/add_grupo")]
async fn add_grupo(
    form: web::types::Form<AddGrupo>,
    session: Session,
) -> impl Responder {
    let form: AddGrupo = form.into_inner();
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let usuario_id: &_ = &form.usuario_id;
    let perfil_id = &form.perfil_id;
    let novogrupo = grupos::cadastra_grupo()
        .bind(&conexao, &usuario_id, &perfil_id)
        .opt()
        .await
        .unwrap()
        .unwrap();
    
    let ctx = context!(flash, msg_error, novogrupo);
    render_minijinja("dashboard/auth/add_grupo.html", ctx)
}


#[web::get("/dashboard/auth/lista_funcionalidades")]
async fn lista_funcionalidades(
    session: Session,
) -> impl Responder {
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let funcionalidades = funcionalidade::todas_funcionalidades()
        .bind(&conexao)
        .all()
        .await
        .unwrap();
    
    let ctx = context!(flash, msg_error, funcionalidades);
    render_minijinja("dashboard/auth/lista_funcionalidades.html", ctx)
}

#[web::post("/dashboard/auth/lista_funcionalidades")]
async fn update_funcionalidade(
    form: web::types::Form<FormEditFuncionalidade>,
    session: Session,
) -> impl Responder {
    let form = form.into_inner();
    session.set("form_signup", &form).unwrap();
    let pool = create_pool().await.unwrap();
    let client = pool.get().await.unwrap();
    let id = &form.id;
    let funcionalidade: &String =  &form.funcionalidade;
    let descricao: &String =  &form.descricao;
    let funcionalidade: funcionalidade::AtualizaFuncionalidade = cornucopia::queries::funcionalidade::atualiza_funcionalidade()
            .bind(&client, &funcionalidade, &descricao, &id)
            .opt()
            .await
            .unwrap()
            .unwrap();
        dbg!(funcionalidade);
        session
            .set("flash", "Funcionalidade atualizada com sucesso.")
            .unwrap();
        return web::HttpResponse::Found()
            .set_header(header::LOCATION, "/dashboard/auth/lista_funcionalidades")
            .finish();
}




#[web::get("/dashboard/auth/lista_permissoes")]
async fn lista_permissoes(
    session: Session,
) -> impl Responder {
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let permissoes = permissao::todas_permissoes()
        .bind(&conexao)
        .all()
        .await
        .unwrap();
    
    let ctx = context!(flash, msg_error, permissoes);
    render_minijinja("dashboard/auth/lista_permissoes.html", ctx)
}

#[web::get("/dashboard/auth/lista_perfis")]
async fn lista_perfis(
    session: Session,
) -> impl Responder {
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let perfis = perfil::todos_perfis()
        .bind(&conexao)
        .all()
        .await
        .unwrap();
    
    let ctx = context!(flash, msg_error, perfis);
    render_minijinja("dashboard/auth/lista_perfis.html", ctx)
}

#[web::get("/dashboard/auth/edit_perfil/{usuario_id}")]
async fn edit_perfil(
    session: Session,
    path: web::types::Path<(i32,)>,
) -> impl Responder {
    let path = path.into_inner();
    println!("{}", path.0);
    let perfil_id = path.0;
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let form_errors = ErrorsSignup::default();
    let form = perfil::perfil_by_id()
        .bind(&conexao, &perfil_id)
        .one()
        .await
        .unwrap();
    session.remove("flash");
    session.remove("msg_error");
    let ctx = context!(flash, msg_error, form_errors, form);
    render_minijinja("dashboard/auth/edit_perfil.html", ctx)
}

#[web::get("/dashboard/auth/edit_permissao/{perfil_id}")]
async fn edit_permissao(
    session: Session,
    path: web::types::Path<(i32,)>,
) -> impl Responder {
    let path = path.into_inner();
    println!("{}", path.0);
    let perfil_id = path.0;
    let pool = create_pool().await.unwrap();
    let conexao = pool.get().await.unwrap();
    let flash: &String = &session
        .get::<String>("flash")
        .unwrap()
        .unwrap_or("".to_string());
    let msg_error = &session
        .get::<String>("msg_error")
        .unwrap()
        .unwrap_or("".to_string());
    let form_errors = ErrorsSignup::default();
    let funcionalidades = funcionalidade::todas_funcionalidades()
        .bind(&conexao)
        .all()
        .await
        .unwrap();
    let permissoes = permissao::permissao_perfil()
        .bind(&conexao, &perfil_id)
        .all()
        .await
        .unwrap();
    dbg!(&funcionalidades);
    dbg!(&permissoes);
    
    #[derive(Serialize, Deserialize)]
    struct PermissaoPerfil {
        pub id:i32,
        pub descricao:String,
        pub funcionalidade:String,
        pub checked:bool
    }

    let mut form = Vec::new();
    for f in funcionalidades {
        let checked = match permissoes.contains(&f.id){
            true=> true,
            false=> false,        
        };
        let p: PermissaoPerfil = PermissaoPerfil{
            id:f.id.clone(),
            descricao:f.descricao,
            checked:checked,
            funcionalidade:f.funcionalidade,
        };
        form.push(p)
    }

    session.remove("flash");
    session.remove("msg_error");
    let ctx = context!(flash, msg_error, form_errors, permissoes, form, perfil_id);
    render_minijinja("dashboard/auth/edit_permissao.html", ctx)
}


#[web::post("/dashboard/auth/update_permissao/{perfil_id}")]
async fn update_permissao(
    // form: web::types::Form<FormEditPerfil>,
    payload: String, 
    session: Session,
    path: web::types::Path<(i32,)>,
) -> impl Responder {
    // let form = form.into_inner();
    let path = path.into_inner();
    println!("{}", path.0);
    let perfil_id = path.0;
    dbg!(&payload);
    let funcionalidades = &payload.split("&").collect::<Vec<_>>();
    dbg!(&funcionalidades);
    let mut permissoes : Vec<i32> = Vec::new();
    // let f: &Vec<&str> = funcionalidades;
    for r in funcionalidades {
        let v = r.split("=").collect::<Vec<_>>()[1];
        let f = r.split("=").collect::<Vec<_>>()[0];
        if f == "funcionalidade" {
            permissoes.push(v.parse::<i32>().unwrap());
        }
    }
    dbg!(&permissoes);
    
    let pool = create_pool().await.unwrap();
    let client = pool.get().await.unwrap();

    for i in &permissoes {
        crate::cornucopia::queries::permissao::insert_permissao()
            .bind(&client, &perfil_id, &i)
            .opt()
            .await
            .unwrap();
    }

    let rows = permissao::permissao_perfil()
        .bind(&client, &perfil_id)
        .all()
        .await
        .unwrap();

    for funcionalidade in rows {
        if !permissoes.contains(&funcionalidade) {
            crate::cornucopia::queries::permissao::delete_permissao()
                .bind(
                    &client,
                    &funcionalidade,
                    &perfil_id,
                )
                .await
                .unwrap();
        }
    }


        let url = format!("/dashboard/auth/edit_permissao/{perfil_id}");
        return web::HttpResponse::Found()
            .set_header(header::LOCATION, url)
            .finish();
    // }
    //final tratamento de erro
}


// #[post("/admin/permissions_update")]
// pub async fn permissions_update(
//     body: web::Bytes,
//     data: web::Data<AppState>,
//     session: Session,
//     req: HttpRequest,
// ) -> Result<impl Responder, Error> {
//     // let _operator = AuthOperator::get_operator(&session, &data, &req).await;
 
//     let body = String::from_utf8(body.to_vec()).unwrap();
//     //println!("body{:#?}", &body);
//     let v = body.replace("[", "").replace("]", "");
//     let v: Vec<&str> = v.split("},{").collect::<Vec<_>>();
//     let mut permissoes: Vec<i32> = Vec::new();
//     let mut group_id: i32 = 0;
//     //println!("v:{:#?}", &v);
//     for line in &v {
//         let fields = line.split(",").collect::<Vec<_>>();
//         let field = fields[0].split(':').collect::<Vec<_>>()[1];
//         let value = fields[1].split(':').collect::<Vec<_>>()[1];
//         let field = field.replace('\"', "").replace("\\", "");
//         let value = value.replace('\"', "").replace("\\", "").replace('}', "");
//         //println!("field:{:#?} value{:#?}", &field, &value);
//         if field == "group_id" {
//             group_id = value.parse::<i32>().unwrap();
//         } else if field == "permissoes" {
//             permissoes.push(value.parse::<i32>().unwrap());
//         }
 
//         //println!("name {:#?}", &field);
 
//         //println!("value {:#?}", &value);
//     }
//     //println!("tenant_id {:#?}", &tenant_id);
 
//     //println!("permissoes{:#?}", &permissoes);
//     for i in &permissoes {
//         crate::cornucopia::queries::auth_permissions::insert_auth_permission()
//             .bind(&data.pool.get().await.unwrap(), &group_id, &i)
//             .await
//             .unwrap();
//     }
 
//     let rows = AuthPermissions::get_by_group(&data, group_id).await;
//     for row in rows {
//         if !permissoes.contains(&row.funcionality_id) {
//             crate::cornucopia::queries::auth_permissions::delete_permissions()
//                 .bind(
//                     &data.pool.get().await.unwrap(),
//                     &group_id,
//                     &row.funcionality_id,
//                 )
//                 .await
//                 .unwrap();
//         }
    
//     }
//     // Ok(HttpResponse::Ok().json("Permissões atualizadas com sucesso!"))
//     session
//         .insert("flash", "Funcionalidade salva com sucesso")
//         .unwrap();
 
//     return Ok(HttpResponse::Found()
//         .append_header((header::LOCATION, format!("/admin/funcionality")))
//         .finish());
 
// }