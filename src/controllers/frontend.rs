use ntex::web::{self, Responder};
//add minijinjua stuff
use minijinja::{self,context};
use serde::{Deserialize, Serialize};
use ntex_session::Session;
use crate::utils::templates::render_minijinja;
use crate::utils::db::create_pool;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(index)
    .service(index)
    .service(viewpost)
    .service(about)
    .service(contact);
}

#[web::get("/")]
async fn index(
    session: Session,
    // data: web::types::State<AppState>,
) -> impl Responder {
    //pega da sessão o número de vezes que a página foi visitada e adiciona mais um
    let visits = session.get::<i32>("visits").unwrap_or(Some(0)).unwrap_or(0) + 1;
    //armazena na sessão a contagem de visitas
    session.set("visits", visits).unwrap();
    let pool = create_pool().await.unwrap();
    let _client = pool.get().await.unwrap();
    let usuario = crate::controllers::dashboard::auth::AuthUser::default();
    // let usuario: Option<crate::cornucopia::queries::usuario::LoginUsuario> = crate::cornucopia::queries::usuario::login_usuario()
    // .bind(&client, &"11111111111", &"esportes")
    // .opt()
    // .await
    // .unwrap();
    //mensagem em caso de sucesso
    let flash = "Está dando tudo certo";
    //mensagem em caso de erro
    let msg_error = "Deu algum problema";
    
    #[derive(Serialize,Deserialize)]
    struct Author{
        name:String,
        email:String,
        date:String
    }

    let author = Author{
        name:"Thiago".to_string(),
        email:"t.souzasantana@gmail.com".to_string(),
        date:"20 de junho de 2024".to_string(),
    };


    let ctx = context!(flash, msg_error, author, visits, usuario);
    render_minijinja("frontend/index.html", ctx)
}


#[web::get("/post.html")]
async fn viewpost() -> impl Responder {



    let flash = "Está dando tudo certo";
    let msg_error = "Deu algum problema";
    
    #[derive(Serialize,Deserialize)]
    struct Author{
        name:String,
        email:String,
        date:String
    }

    let author = Author{
        name:"Thiago".to_string(),
        email:"t.souzasantana@gmail.com".to_string(),
        date:"20 de junho de 2024".to_string(),
    };


    let ctx = context!(flash, msg_error, author);
    render_minijinja("frontend/post.html", ctx)
}

#[web::get("/about.html")]
async fn about() -> impl Responder {



    let flash = "Está dando tudo certo";
    let msg_error = "Deu algum problema";
    
    #[derive(Serialize,Deserialize)]
    struct Author{
        name:String,
        email:String,
        date:String
    }

    let author = Author{
        name:"Thiago".to_string(),
        email:"t.souzasantana@gmail.com".to_string(),
        date:"20 de junho de 2024".to_string(),
    };


    let ctx = context!(flash, msg_error, author);
    render_minijinja("frontend/about.html", ctx)
}

#[web::get("/contact.html")]
async fn contact() -> impl Responder {



    let flash = "Está dando tudo certo";
    let msg_error = "Deu algum problema";
    
    #[derive(Serialize,Deserialize)]
    struct Author{
        name:String,
        email:String,
        date:String
    }

    let author = Author{
        name:"Thiago".to_string(),
        email:"t.souzasantana@gmail.com".to_string(),
        date:"20 de junho de 2024".to_string(),
    };


    let ctx = context!(flash, msg_error, author);
    render_minijinja("frontend/contact.html", ctx)
}
