# remove os serviços anteriores, se existirem
rm /etc/systemd/system/thiago.service

# cria um link simbólico do serviço
ln -s /home/thiago/projetos/thiago/services/thiago.service /etc/systemd/system/thiago.service

# habilita o serviço
systemctl enable thiago.service

#recarrega o daemon
systemctl daemon-reload

# reinicia o serviço
service thiago restart

# service thiago start
# service thiago stop