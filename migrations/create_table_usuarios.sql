create table usuario (
	id serial,
	nome text not null,
	email text not null,
	cpf text not null,
	senha text not null,
	is_active varchar(1) not null default 'T',
	created_at timestamp not null default now(),
	created_by int not null default 1,
	updated_at timestamp not null default now(),
	updated_by int not null default 1,
	unique(email),
	unique(cpf)
);

create table funcionalidade (
	id serial,
	funcionalidade text not null,
	descricao text not null
);

create table perfil (
	id serial,
	perfil text not null,
	descricao text not null
);

create table permissao (
	perfil_id int not null,
	funcionalidade_id int not null,
	unique (perfil_id, funcionalidade_id)
);

create table grupo (
	usuario_id int not null,
	perfil_id int not null,
	unique (usuario_id, perfil_id)

)